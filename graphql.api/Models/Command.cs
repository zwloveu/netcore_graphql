using System;

namespace Graphql.API.Models
{
    public class Command
    {
        public byte Sequence { get; set; }

        public string Description => Sequence switch 
        {
            <= 0 => "UnAvailable",
            > 0 and <= 50 => "Normal",
            > 50 and <= 100 => "High",
            > 100 and <= byte.MaxValue => "Golden"
        };
    }
}
