using System;
using Graphql.API.Models;
using GraphQL.Types;

namespace Graphql.API.GraphQL.Types
{
    public class CommandType : ObjectGraphType<Command>
    {
        public CommandType()
        {
             Field(t => t.Sequence, type: typeof(IdGraphType));
             Field(t => t.Description);
        }
    }
}
