using System;
using GraphQL.Types;
using GraphQL.Utilities;
using Graphql.API.GraphQL.Types;
using Microsoft.Extensions.DependencyInjection;

namespace Graphql.API.GraphQL
{
    public class WeatherForecastSchema : Schema
    {
        public WeatherForecastSchema(IServiceProvider provider)
            : base(provider)
        {
            Query = provider.GetRequiredService<WeatherForecastQuery>();
        }
    }
}
