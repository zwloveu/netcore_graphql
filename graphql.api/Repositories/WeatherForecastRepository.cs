using System;
using System.Collections.Generic;
using System.Linq;
using Graphql.API.Models;

namespace Graphql.API.Repositories
{
    public class WeatherForecastRepository : IWeatherForecastRepository
    {
        private readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        public IEnumerable<WeatherForecastModel> GetAll()
        {
            var rng = new Random();
            return Enumerable.Range(1, 5).Select(index => new WeatherForecastModel
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)],
                CommandSequence = (byte)rng.Next(0, byte.MaxValue)
            })
            .ToArray();
        }
    }
}
