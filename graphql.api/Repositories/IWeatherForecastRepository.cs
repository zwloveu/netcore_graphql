using System;
using System.Collections.Generic;
using Graphql.API.Models;

namespace Graphql.API.Repositories
{
    public interface IWeatherForecastRepository
    {
        IEnumerable<WeatherForecastModel> GetAll();
    }
}
