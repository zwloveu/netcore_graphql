using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using GraphQL.Types;
using GraphQL.Server.Ui.Playground;
using Graphql.API.Application.WeatherForecast;
using Graphql.API.Repositories;
using Graphql.API.GraphQL;
using GraphQL.Server;
using Graphql.API.GraphQL.Types;
using Graphql.API.Application.WeatherForecast.Graph;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;

namespace Graphql.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options => options.AddPolicy("AllowAllOrigins", builder =>
                builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader()));

            services.AddHealthChecks();

            services.AddSingleton<ICommandCache, CommandCache>();
            services.AddSingleton<IWeatherForecastRepository, WeatherForecastRepository>();
            services.AddSingleton<IWeatherForecastManager, WeatherForecastManager>(); 
            services.AddSingleton<WeatherForecastQuery>();
            services.AddSingleton<ISchema, WeatherForecastSchema>();

            services.AddGraphQL(options => options.EnableMetrics = false)
                .AddGraphTypes()
                .AddSystemTextJson(
                    deserializerSettings => { deserializerSettings.PropertyNameCaseInsensitive = true; })
                .AddErrorInfoProvider(opt => opt.ExposeExceptionStackTrace = true);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseCors("AllowAllOrigins");

            app.UseHealthChecks("/health", new HealthCheckOptions()
            {
                Predicate = (check) => check.Tags.Contains("ready"),
            });

            app.UseGraphQL<ISchema>();
            app.UseGraphQLPlayground();
        }
    }
}
