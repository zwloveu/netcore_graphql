using System;
using Graphql.API.Models;
using System.Collections.Generic;

namespace Graphql.API.Application.WeatherForecast.Graph
{
    public interface IWeatherForecastManager
    {
        IEnumerable<WeatherForecastModel> GetAll();
        IEnumerable<WeatherForecastModel> GetByCommand(byte commandSequence);
    }
}