using System;
using System.Collections.Generic;
using System.Linq;
using Graphql.API.Models;
using Graphql.API.Repositories;
using Microsoft.Extensions.Logging;

namespace Graphql.API.Application.WeatherForecast.Graph
{
    public class WeatherForecastManager : IWeatherForecastManager
    {
        private readonly ILogger<WeatherForecastManager> _logger;
        private readonly ICommandCache _commandCache;
        private readonly IWeatherForecastRepository _weatherForecastRepository;
        private readonly List<WeatherForecastModel> _wfs = new List<WeatherForecastModel>();  

        public WeatherForecastManager(
            ILogger<WeatherForecastManager> logger,
            ICommandCache commandCache, 
            IWeatherForecastRepository weatherForecastRepository)
        {
            _logger = logger;
            _commandCache = commandCache;
            _weatherForecastRepository = weatherForecastRepository;

            _commandCache.AddOrUpdateCommand(new Command { Sequence = 0 });
        }

        public IEnumerable<WeatherForecastModel> GetAll()
        {
            var wfs = _weatherForecastRepository.GetAll();

            if (wfs != null && wfs.Any())
            {
                foreach (var wf in wfs)
                {
                    _commandCache.AddOrUpdateCommand(new Command { Sequence = wf.CommandSequence });
                }
            }

            _wfs.AddRange(wfs);

            _logger.LogInformation($"Total {_wfs.Count()}");

            return wfs;
        }

        public IEnumerable<WeatherForecastModel> GetByCommand(byte commandSequence)
        {
            return _wfs.Where(o => o.CommandSequence == commandSequence);
        }
    }
}