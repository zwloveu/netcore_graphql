using System;
using System.Collections.Generic;
using Graphql.API.Models;

namespace Graphql.API.Application.WeatherForecast
{
    public class CommandCache : ICommandCache
    {
        private readonly Dictionary<byte, Command> _commands = new Dictionary<byte, Command>();      

        public Command GetCommand(byte? sequence)
        {
            if (sequence == null) return null;
            return _commands.TryGetValue(sequence.Value, out var command) ? command : null;
        }

        public void AddOrUpdateCommand(Command command)
        {
            _commands[command.Sequence] = command;
        }

        public void AddOrUpdateCommands(IEnumerable<Command> Commands)
        {
            foreach (var command in Commands)
            {
                AddOrUpdateCommand(command);
            }
        }

        public void RemoveCommand(byte sequence)
        {
            _commands.Remove(sequence);
        }
    }
}