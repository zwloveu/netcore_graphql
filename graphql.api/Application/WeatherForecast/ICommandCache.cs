using System;
using System.Collections.Generic;
using Graphql.API.Models;

namespace Graphql.API.Application.WeatherForecast
{
    public interface ICommandCache 
    {
        Command GetCommand(byte? sequence);
        void AddOrUpdateCommand(Command command);
        void AddOrUpdateCommands(IEnumerable<Command> Commands);
        void RemoveCommand(byte sequence);
    }
}