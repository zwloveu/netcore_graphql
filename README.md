# DOTNET WITH GRAPHQL

## Query Sample in GraphQLPlayground default path: /ui/playground
```
query WeatherForecastQuery {
  allwfs {
    date
  	temperatureC
  	temperatureF
  	summary
  	commandSequence
    command {
      sequence
      description
    }
  }
  wfswithcommand(command: 127) {
    date
    temperatureC
    commandSequence
  }
}
```